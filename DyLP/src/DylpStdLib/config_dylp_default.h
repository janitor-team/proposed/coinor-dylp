
/***************************************************************************/
/*           HERE DEFINE THE PROJECT SPECIFIC PUBLIC MACROS                */
/*    These are only in effect in a setting that doesn't use configure     */
/***************************************************************************/

/* Version number of project */
#define DYLP_VERSION "1.10.4"

/* Major Version number of project */
#define DYLP_VERSION_MAJOR 1

/* Minor Version number of project */
#define DYLP_VERSION_MINOR 10

/* Release Version number of project */
#define DYLP_VERSION_RELEASE 4

/*
  Define to the C type corresponding to the C++ bool type. `char' is
  correct on many systems. The next most likely choice is int.
*/
#define BOOL char
